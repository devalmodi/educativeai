import React from "react";
import "App.css";
import Navbar from "react-bootstrap/Navbar";
import logo from "art/educative_ai_logo/1-square.jpg";

function HeaderContainer() {
  return (
    <>
      <Navbar
        style={{
          width: "100%",
          // backgroundColor: "#28527a",
          backgroundImage: "linear-gradient(45deg, #7b2be8, #3284d7)",
          paddingTop: 0,
          paddingBottom: 0,
          paddingLeft: "8px",
        }}
        variant="dark"
      >
        <Navbar.Brand>
          <img
            alt=""
            src={logo}
            width="50"
            height="50"
            className="d-inline-block align-top"
          />{" "}
          <span className="appLogo">Educative AI</span>
        </Navbar.Brand>
      </Navbar>
    </>
  );
}

export default HeaderContainer;
